<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Contact'] = 'Əlaqə';
$lang['About_us'] = 'HAQQIMIZDA';
$lang['Professional_staff'] = 'Peşəkar heyət';
$lang['Correct_application_of_projects'] = 'Layihələrin düzgün tətbiqi';
$lang['Safe_work_environment'] = 'Təhlükəsiz iş mühiti';
$lang['GALLERY'] = 'QALERİYA';
$lang['Projects'] = 'Layihələr';
$lang['Safe_working_hours'] = 'Təhlükəsiz iş saatları';
$lang['Employees'] = 'İşçi heyəti';
$lang['Our_partners'] = 'Partnyorlarımız';
$lang['CONTACT_INFO'] = 'ƏLAQƏ MƏLUMATI';
$lang['Address'] = 'Ünvan';
$lang['Phone'] = 'Telefon';
$lang['E-mail'] = 'E-poçt';
$lang['Our_purpose'] = 'MƏQSƏDİMİZ';
$lang['Address_text'] = 'Süleyman Sani Axundov küçəsi, 3123, Union park';
$lang['about_text'] = '"Kamur Group" MMC şirkəti 2018-ci ildə təsis edilmişdir.
  Şirkətimiz inşaat sektorunda ixtisaslaşmış olub, əsas fəaliyyət sahəsi dəmir-beton quraşdırma və qəlibləmə işləridir.
  Bununla yanaşı təklif etdiyimiz xidmətlərdə hörgü-suvaq, elektrik və su təchizatının təmin edilməsi,
  təhlükəsizlik sistemlərinin qurulması kimi digər fəaliyyət sahələri də mövcuddur.
  Şirkətimizin yaranma tarixi hər nə qədər yeni olsada, təsisçilərin və şirkətin qeydiyyatında olan personal heyətinin təcrübəsi 10 ildən artıqdır.';
$lang['purpose_text'] = 'Bir inşaat şirkəti olaraq əsas məqsədimiz – şirkətimizin adı altında bir arada toplaşmış peşəkar mütəxəssislərin effektiv və sürətli iş rejimi qurub , yüksək keyfiyyətli binalar inşa etməklə ölkəmizin bütün şəhərlərinin çiçəklənməsi və göz oxşayan görünüş alması məqsədi ilə müasir üslubda bina və evlərin inşa edilməsində bilavasitə yaxından iştirak etməkdir.Təbii ki, sürətlə davam edən inşaat prosessində əməyin təhlükəsizliyinin də təmin olunması şirkətimizin öndər məqsədlərindən biridir. Nəinki, durmadan inkişaf etməkdə olan Azərbaycan Respublikası, eləcə də onun hüdudlarından kənarda böyük layihələrə imza ataraq beynəlxalq standartlara cavab verən dünyanın qabaqcıl şirkətləri siyahısında olmaq və bununla ölkəmizi bütün dünyada tərənnüm etmək şirkətimizin ən başlıca hədəfidir.';













//
