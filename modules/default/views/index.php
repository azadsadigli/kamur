<!DOCTYPE html>
<html lang="az">
<head>
    <meta charset="utf-8">
    <title><?= "Kamur group" ?></title>
    <meta name="description" content="Template built for Construction Company, Building Services, Architecture, Engineering, Cleaning Service and other Construction related services">
    <meta name="keywords" content=" architecture, builder, building, building company, cleaning services, construction, construction business, construction company">
    <meta name="author" content="Kamur group">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= assets("favicon.ico/apple-icon-57x57.png")?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= assets("favicon.ico/apple-icon-60x60.png")?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= assets("favicon.ico/apple-icon-72x72.png")?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= assets("favicon.ico/apple-icon-76x76.png")?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= assets("favicon.ico/apple-icon-114x114.png")?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= assets("favicon.ico/apple-icon-120x120.png")?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= assets("favicon.ico/apple-icon-144x144.png")?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= assets("favicon.ico/apple-icon-152x152.png")?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= assets("favicon.ico/apple-icon-180x180.png")?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= assets("favicon.ico/android-icon-192x192.png")?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= assets("favicon.ico/favicon-32x32.png")?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= assets("favicon.ico/favicon-96x96.png")?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= assets("favicon.ico/favicon-16x16.png")?>">
    <link rel="manifest" href="<?= assets("favicon.ico/manifest.json")?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="/assets/css/master.css">
</head>

<body class="front-page no-sidebar site-layout-full-width header-style-5 menu-has-cart header-sticky">

<div id="wrapper" class="animsition">
<div id="page" class="clearfix">

<div id="site-header-wrap">
    <!-- Header -->
    <header id="site-header" class="header-front-page style-5">
        <div id="site-header-inner" class="container">
            <div class="wrap-inner">
                <div id="site-logo" class="clearfix">
                    <div id="site-logo-inner">
                        <a href="/" title="Construction" rel="home" class="main-logo">
                            <img src="/assets/images/logo/kamur.png" alt="Construction" data-retina="/assets/images/logo/kamur.png" >
                        </a>
                    </div>
                </div>

                <div class="mobile-button">
                  <span></span>
                </div>

                <nav id="main-nav" class="main-nav">
                    <ul class="menu">
                        <li class="menu-item"><a href="https://api.whatsapp.com/send?phone=994502104630"><?= lang("Contact") ?></a></li>
                    </ul>
                </nav><!-- /#main-nav -->
            </div>
        </div><!-- /#site-header-inner -->
    </header><!-- /#site-header -->
</div><!-- /#site-header-wrap -->

<!-- Slider -->
<div class="rev_slider_wrapper fullwidthbanner-container">
    <div id="rev-slider3" class="rev_slider fullwidthabanner">
        <ul>
            <!-- Slide -->
            <li data-transition="random">
                <!-- Main Image -->
                <img src="/assets/images/photo31dark.jpg" alt="" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['58','54','50','46']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="o:0"
                    data-transform_out="o:0"
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    BUILDING ISN'T JUST A JOB<br>IT IS OUR <span class="text-accent-color">PASSION</span>.
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['18','16','16','16']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="o:0"
                    data-transform_out="o:0"
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    Quality work at Construction Company  is achieved through careful planning,<br>dedication, and skilled execution.
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;"
                    data-transform_in="o:0"
                    data-transform_out="o:0"
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                </div>
            </li>
            <!-- End Slide -->
            <!-- Slide -->
            <li data-transition="random">
                <!-- Main Image -->
                <img src="/assets/images/photo23darkd.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow text-center font-weight-600"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['60','54','50','46']"
                    data-lineheight="['70','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="o:0"
                    data-transform_out="o:0"
                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                    data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    INSDUSTRY RECOGNITION
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow text-center"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['32','42','52','62']"
                    data-fontsize="['18','16','16','16']"
                    data-lineheight="['28','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;"
                    data-transform_in="o:0"
                    data-transform_out="o:0"
                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                    data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    We are proud to have been honored with numerous local<br>and national awards over the years.
                </div>
                <div class="sfb tp-caption"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['128','138','148','158']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;"
                    data-transform_in="o:0"
                    data-transform_out="o:0"
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                </div>
            </li>
            <!-- End Slide -->
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>

<!-- Main Content -->
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <!-- HOW WE BUILD -->
                    <section class="wprt-section how-we-build">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center margin-bottom-10"><?= lang("About_us") ?></h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="20"></div>

                                    <p class="wprt-subtitle text-center"><?= lang("about_text") ?></p>

                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-3 width-90">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-engineer"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#"><?= lang("Professional_staff") ?></a></h3>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-3 width-90">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-ruler-1"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#"><?= lang("Correct_application_of_projects") ?></a></h3>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-3 width-90">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-garden-fence"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#"><?= lang("Safe_work_environment") ?></a></h3>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                    <!-- WORKS -->
                    <section class="wprt-section works parallax">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center text-white margin-bottom-10"><?= lang("GALLERY") ?></h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="36" data-mobi="36" data-smobi="36"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->

                        <div class="wprt-project arrow-style-2 has-bullets bullet-style-2" data-layout="slider" data-column="4" data-column2="3" data-column3="2" data-column4="1" data-gaph="1" data-gapv="1">
                            <div id="projects" class="cbp">
                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <div class="grid">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo2.jpg" alt="image" />
                                            </figure>
                                            </div>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo2.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo26.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo26.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo4.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo4.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-zoe">
                                                <img src="/assets/images/photo21.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo21.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo15.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo15.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo7.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo7.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo16.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo16.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->
                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo9.jpg" altgalle="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo9.jpg">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->
                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="/assets/images/photo28.jpg" alt="image" />
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="/assets/images/photo28.jpg" >
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->
                            </div><!-- /#projects -->
                        </div><!--/.wprt-project -->

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                    <!-- WHY CHOOSE US -->
                    <!-- <section class="wprt-section why-choose-us">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center margin-bottom-10">WHY CHOOSE US</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="20"></div>

                                    <p class="wprt-subtitle text-center">We are providing the best construction expertise in the industry. We’ve formed three divisions specifically designed to serve their respective markets</p>

                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                </div>

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-drawing"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">Experience & Expertise</a></h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="45" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-engineer"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">On Time & On Budget</a></h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div>

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-drawing-compass"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">Master Builder</a></h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="45" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-drill-2"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">Satisfied Customers</a></h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div>

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-light-bulb"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">Project Management</a></h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="45" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-pipe-11"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">Licensed Building Practioner</a></h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="60" data-mobi="60" data-smobi="60"></div>
                                </div>
                            </div>
                        </div>
                    </section> -->

                    <!-- SERVICES -->
                    <section class="wprt-section service">
                        <div class="container-fluid padding-0">
                            <div class="row margin-0">
                                <div class="col-md-6 padding-0">
                                  <div class="construction" >
                                    <img src="/assets/images/photo38eff.jpg" alt="image">
                                  </div>
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6 padding-0">
                                    <div class="wprt-content-box style-2">
                                        <h2 class="margin-bottom-10"><?= lang("Our_purpose")?></h2>
                                        <div class="wprt-lines style-1 custom-2">
                                            <div class="line-1"></div>
                                        </div>
                                        <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="25"></div>

                                        <p class="wprt-subtitle left"><?= lang("purpose_text") ?></p>

                                        <!-- <ul class="wprt-list style-4 accent-color margin-top-30 margin-bottom-25">
                                            <li>On-Site Supervision & Project Management</li>
                                            <li>Subcontractor Coordination & Real-Time Project Communication</li>
                                            <li>Cost Control Management &Accounting</li>
                                            <li>Security & Scheduling and Phasing</li>
                                            <li>Safety Programming & Drawings Review and Submittal</li>
                                            <li>Total Project Coordination</li>
                                        </ul> -->
                                    </div><!-- /.wprt-content-box -->
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </section>

                    <!-- <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2>DELIVERY METHOD</h2>
                                    <div class="wprt-lines style-1 custom-3">
                                        <div class="line-1"></div>
                                        <div class="line-2"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>

                                    <div class="wprt-toggle style-1">
                                        <h3 class="toggle-title">The Right Delivery Method For You</h3>
                                        <div class="toggle-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a metus pellentesque, scelerisque ex sed, volutpat nisi. Curabitur tortor mi, eleifend ornare lobortis non. Nulla porta purus quis iaculis ultrices. Proin aliquam sem at nibh hendrerit sagittis. Nullam ornare odio eu lacus tincidunt malesuada. Sed eu vestibulum elit. Curabitur tortor mi, eleifend ornare.</div>
                                    </div>

                                    <div class="wprt-toggle active style-1">
                                        <h3 class="toggle-title">Construction Management (CM)</h3>
                                        <div class="toggle-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a metus pellentesque, scelerisque ex sed, volutpat nisi. Curabitur tortor mi, eleifend ornare lobortis non. Nulla porta purus quis iaculis ultrices. Proin aliquam sem at nibh hendrerit sagittis. Nullam ornare odio eu lacus tincidunt malesuada. Sed eu vestibulum elit. Curabitur tortor mi, eleifend ornare.</div>
                                    </div>

                                    <div class="wprt-toggle style-1">
                                        <h3 class="toggle-title">The design-build method brings the architectural</h3>
                                        <div class="toggle-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a metus pellentesque, scelerisque ex sed, volutpat nisi. Curabitur tortor mi, eleifend ornare lobortis non. Nulla porta purus quis iaculis ultrices. Proin aliquam sem at nibh hendrerit sagittis. Nullam ornare odio eu lacus tincidunt malesuada. Sed eu vestibulum elit. Curabitur tortor mi, eleifend ornare.</div>
                                    </div>

                                    <div class="wprt-toggle style-1">
                                        <h3 class="toggle-title">Integrated Project Delivery (IPD)</h3>
                                        <div class="toggle-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a metus pellentesque, scelerisque ex sed, volutpat nisi. Curabitur tortor mi, eleifend ornare lobortis non. Nulla porta purus quis iaculis ultrices. Proin aliquam sem at nibh hendrerit sagittis. Nullam ornare odio eu lacus tincidunt malesuada. Sed eu vestibulum elit. Curabitur tortor mi, eleifend ornare.</div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2>TEAM WORK</h2>
                                    <div class="wprt-lines style-1 custom-3">
                                        <div class="line-1"></div>
                                        <div class="line-2"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>

                                    <div class="wprt-galleries galleries w-570px" data-width="135" data-margin="10">
                                        <div id="wprt-slider" class="flexslider">
                                            <ul class="slides">
                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="/assets/images/photo26.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="/assets/images/photo26.jpg" alt="image" />
                                                </li>

                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="/assets/images/photo27.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="/assets/images/photo27.jpg" alt="image" />
                                                </li>

                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="/assets/images/photo34.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="/assets/images/photo34.jpg" alt="image" />
                                                </li>

                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="/assets/images/photo30.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="/assets/images/photo30.jpg" alt="image" />
                                                </li>
                                            </ul>
                                        </div>

                                        <div id="wprt-carousel" class="flexslider">
                                            <ul class="slides">
                                                <li><img src="/assets/images/photo26.jpg" alt="image"></li>
                                                <li><img src="/assets/images/photo27.jpg" alt="image"></li>
                                                <li><img src="/assets/images/photo34.jpg" alt="image"></li>
                                                <li><img src="/assets/images/photo30.jpg" alt="image"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                </div>
                            </div>
                        </div>
                    </section> -->

                    <!-- FACTS -->
                    <section class="wprt-section facts-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="130" data-mobi="80" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                                <div class="col-md-4">
                                    <div class="wprt-counter text-center white-type has-plus">
                                        <div class="number" data-speed="5000" data-to="8" data-in-viewport="yes">8</div>
	                                    <div class="wprt-lines style-2 custom-1 margin-top-10 margin-bottom-10">
	                                        <div class="line-1"></div>
	                                    </div>
                                        <div class="text"><?= lang("Projects") ?></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->
                                <div class="col-md-4">
                                    <div class="wprt-counter text-center white-type has-plus">
                                        <div class="number" data-speed="5000" data-to="9120" data-in-viewport="yes">9120</div>
                                        <div class="wprt-lines style-2 custom-1 margin-top-10 margin-bottom-10">
	                                        <div class="line-1"></div>
	                                    </div>
                                        <div class="text"><?= lang("Safe_working_hours") ?></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->
                                <div class="col-md-4">
                                    <div class="wprt-counter text-center white-type has-plus">
                                        <div class="number" data-speed="5000" data-to="140" data-in-viewport="yes">140</div>
                                        <div class="wprt-lines style-2 custom-1 margin-top-10 margin-bottom-10">
	                                        <div class="line-1"></div>
	                                    </div>
                                        <div class="text"><?= lang("Employees") ?></div>
                                    </div>
                                </div><!-- /.col-md-4 -->
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="130" data-mobi="80" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>


                    <!-- PARTNERS -->
                    <section id="partners" class="wprt-section partners">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="30" data-mobi="30" data-smobi="30"></div>
                                    <h2 class="partners-title"><?= lang("Our_partners") ?></h2>
                                    <div class="wprt-partners">
                                        <div class="owl-carousel">

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-bosch.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-dewalt.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-pfeifer.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-sait-demirci.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-sveza.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-yds.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" href="#"><img src="/assets/images/partner-wacker-neuson.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" class="alphine" href="#"><img src="/assets/images/partner-alpine.jpeg" alt="image" /></a>
                                            </div>

                                            <div class="partner">
                                                <a target="_blank" class="sika" href="#"><img src="/assets/images/partner-sika.jpeg" alt="image" /></a>
                                            </div>

                                        </div>
                                    </div><!-- /.wprt-partners -->

                                    <div class="wprt-spacer" data-desktop="30" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-12 -->

                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                </div><!-- /.page-content -->
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<footer id="footer">
    <div id="footer-widgets" class="container style-1">
        <div class="row">
            <div class="col-md-6">
                <div class="widget widget_text">
                    <h2 class="widget-title"><span><?= lang("About_us") ?></span></h2>
                    <div class="textwidget">
                        <img src="/assets/images/logo/kamur.png" alt="image" height="200" width="200" class="margin-bottom-25 logo-vertical-fully-white" />
                        <p><?= lang("about_text") ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="widget widget_information">
                    <h2 class="widget-title"><span><?= lang("CONTACT_INFO") ?></span></h2>
                    <ul class="style-2">
                        <li class="address clearfix">
                            <span class="hl"><?= lang("Address") ?>:</span>
                            <span class="text"><?= lang("Address_text") ?></span>
                        </li>
                        <li class="phone clearfix">
                            <span class="hl"><?= lang("Phone") ?>:</span>
                            <span class="text">+99450 210 46 30</span>
                        </li>
                        <li class="email clearfix">
                            <span class="hl"><?= lang("E-mail") ?>:</span>
                            <span class="text"><a href="mailto::office@kamurgroup.az">office@kamurgroup.az</a></span>
                        </li>
                    </ul>
                </div>

                <div class="widget widget_spacer">
                    <div class="wprt-spacer clearfix" data-desktop="10" data-mobi="10" data-smobi="10"></div>
                </div>

                <div class="widget widget_socials">
                    <div class="socials">
                        <a target="_blank" href="https://www.instagram.com/kamur_group/"><i class="fa fa-instagram"></i></a>
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=994502104630"><i class="fa fa-whatsapp"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-globe"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Bottom -->
<div id="bottom" class="clearfix style-1">
    <div id="bottom-bar-inner" class="wprt-container">
        <div class="bottom-bar-inner-wrap">

            <div class="bottom-bar-content">
                <div id="copyright">2018 - <?=date("Y");?> © <span class="text-accent-color">Kamur Group</span>
                </div><!-- /#copyright -->
            </div><!-- /.bottom-bar-content -->

            <!-- <div class="bottom-bar-menu">
                <ul class="bottom-nav">
                    <li><a href="#/">HISTORY</a></li>
                    <li><a href="#/">FAQ</a></li>
                    <li><a href="#/">EVENTS</a></li>
                </ul>
            </div> -->
            <!-- /.bottom-bar-menu -->
        </div>
    </div>
</div>

</div><!-- /#page -->
</div><!-- /#wrapper -->

<a id="scroll-top"></a>

<!-- Javascript -->
<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/animsition.js"></script>
<script type="text/javascript" src="/assets/js/plugins.js"></script>
<script type="text/javascript" src="/assets/js/countTo.js"></script>
<script type="text/javascript" src="/assets/js/flexslider.js"></script>
<script type="text/javascript" src="/assets/js/owlCarousel.js"></script>
<script type="text/javascript" src="/assets/js/cube.portfolio.js"></script>
<script type="text/javascript" src="/assets/js/main.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="/assets/js/rev-slider.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/assets/css/rev-slider/js/extensions/revolution.extension.video.min.js"></script>

</body>
</html>
